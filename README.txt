# Introduction

* Provides a new content type "webform_collection" (using a feature).
* Components in these nodes act as collection which can be re-used in other webforms.
* Adds a select option at the bottom of any other webform.
* Using this interface, collections of pre-defined components can be added with a simple click.
* Conditionals are also transferred.
* Optionally import default webform collections, ready to use.

# Install

* All functionality is currently available as a feature in the DvG Beta package.

* This module contains 2 modules:
** dvg_webform_component_collections (main module)
** dvg_webform_component_collections_defaults (import)

* During install of 'defaults'', default webform collections will be imported and ready for use.